package node

type Node struct {
	Ip           string // 节点IP地址
	Port         int    // 节点端口
	Hostname     string // 节点host
	Weight       int    // 节点权重
	CurWeight    int    // 节点临时权重
	EffectWeight int    // 节点有效权重
}
