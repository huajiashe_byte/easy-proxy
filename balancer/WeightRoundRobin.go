package balancer

import "easy-proxy/node"

type WeightRoundRobin struct {
	Nodes []*node.Node
}

func (r *WeightRoundRobin) Next() *node.Node {
	var n *node.Node
	total := 0
	for i := 0; i < len(r.Nodes); i++ {
		w := r.Nodes[i]
		total += w.EffectWeight
		w.CurWeight += w.EffectWeight
		if w.EffectWeight < w.Weight {
			w.EffectWeight++
		}
		if n == nil || w.CurWeight > n.CurWeight {
			n = w
		}
	}
	if n == nil {
		return nil
	}

	n.CurWeight -= total
	return n
}
