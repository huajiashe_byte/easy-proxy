package balancer

import (
	"easy-proxy/node"
)

type RoundRobin struct {
	CurIndex int
	Nodes    []*node.Node
}

func (r *RoundRobin) Next() *node.Node {
	if len(r.Nodes) == 0 {
		return nil
	}
	l := len(r.Nodes)
	if r.CurIndex >= l {
		r.CurIndex = 0
	}
	currAddr := r.Nodes[r.CurIndex]
	r.CurIndex = (r.CurIndex + 1) % l
	return currAddr
}
