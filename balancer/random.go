package balancer

import (
	"easy-proxy/node"
	"math/rand"
)

type Random struct {
	CurIndex int
	Nodes    []*node.Node
}

func (r *Random) Next() *node.Node {
	if len(r.Nodes) == 0 {
		return nil
	}
	r.CurIndex = rand.Intn(len(r.Nodes))
	return r.Nodes[r.CurIndex]
}
